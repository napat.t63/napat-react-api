import "./App.css";
import { useState } from "react";
import Navbar from "./Navbar";
import Users from "./Users";
import Axios from "axios";

function App() {
  const [name, setName] = useState("");
  const [weight, setWeight] = useState(0);
  const [amount, setAmount] = useState(0);
  const [avatar, setAvatar] = useState("");
  const [usersList, setUsersList] = useState([]);

  const getUsers = () => {
    Axios.get("https://napat-node-crud.herokuapp.com/users").then((response) => {
      setUsersList(response.data);
    });
  };

  const addUser = () => {
    Axios.post("https://napat-crud-api.herokuapp.com/create", {
      name: name,
      weight: weight,
      avatar: avatar,
      amount: amount,

    }).then(() => {
      setUsersList([
        ...usersList,
        {
          name: name,
          weight: weight,
          avatar: avatar,
          amount: amount,
    
        },
      ]);
    });
  };

  

  return (
    <div>
      <Navbar />
      <div className="App container">
        <h1>meue</h1>
        <div className="meue">
          <form action="">
            <div className="mb-3">
              <label className="form-label" htmlFor="name">
                Name:
              </label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter name"
                onChange={(event) => {
                  setName(event.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="weight">Weight:</label>
              <input
                type="number"
                className="form-control"
                placeholder="Enter weight"
                onChange={(event) => {
                  setWeight(event.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="amount">Amount:</label>
              <input
                type="number"
                className="form-control"
                placeholder="Enter amount"
                onChange={(event) => {
                  setAmount(event.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="link image">avatar:</label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter link image"
                onChange={(event) => {
                  setAvatar(event.target.value);
                }}
              />
            </div>
            <button class="btn btn-success" onClick={addUser}>
              Add meue
            </button>
          </form>
        </div>
        <hr />
        <div className="users">
          <button class="btn btn-primary" onClick={getUsers}>
            Show meue
          </button>
          <br />
          <br />
          {usersList.map((val, key) => {
            return (
              <div className="ีusers card">
                <div className="card-body text-left">
                  <p className="card-text">Name: {val.name}</p>
                  <p className="card-text">weight: {val.weight}</p>
                  <p className="card-text">amount: {val.amount}</p>
                  <p className="card-text">avatar: {val.avatar}</p>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      <Users />
    </div>
  );
}

export default App;
